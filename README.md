# GIT

## What is Git ?

### 1. Version Control
* Git is referred to as a Version Control System
* It allows you to move back and forth between different versions, to compare the different versions and to see what changed between each one

### 2. Distributed version control
* Different users maintain their own repositories instead of working from a central repository

### 3. Distributed version control
* Different users maintain their own repositories instead of working from a central repository

### 4. Who should use Git
* Anyone wanting to track edits
* Anyone needing to share changes with collaborators
* Not useful for tracking non-text files

## Installing Git ?

### 1. Installing Git
* Download from https://git-scm.com/downloads
* which git (Return path to git)
* git --version (Return version of git)

### 2. Configuring Git

* System configuration (Apply for all users)
```
    + System (Apply for all users)
        Unix: /etc/gitconfig  
        Windows: Program FIles\Git\etc\gitconfig
        git config --system  
    + User (Apply for 1 user)
        Unix: ~/.gitconfig  
        Windows: $HOME\.gitconfig  
        git config --global  
    + Project (Apply for 1 project)
        my_project/.git/config
        git config
```
* Git's useful config
```
    + Default text ediot
        git config --global core.editor <name_of_editor>
    + Git output colors
        git config --global color.ui true
```
* Git help
```
    + git help <command>
    + Press F for Forward, B for Backward, Q for Quit
```

## Getting started

### 1. Initializing a repository
```
git init
```

### 2. Understanding where Git files are stored
```
ls -la .git
```

### 3. First commit
```
git add . (Add all the changes)
git commit -m <message>
```

### 4. Commit messages
* Each line < 72 characters
* Optionally followed by a blank line
* Write commit message in present tense
* Add "ticket tracking number"
* Use shorthands: "[css, js]", "bugfix"

### 5. Viewing the commit log
```
git log
git log -n (Return n commits)
git log --since=<date> (Return commits since date)
git log --until=<date> (Return commits until date)
git log --author="<author>" (Return commits of author)
git log --grep=<text> (Return commits whose commit message match text)
```

## Git concepts and architecture

### 1. Three-tress architecture
* working <--> staging index <--> repository

### 2. Git workflow
* git add will add our files to the staging index
* From the staging we will commit those changes to the repostiroy

### 3. Using hash values
* Git generates a checksum for each change set
* data integrity is fundamental
* Git uses SHA-1 hash algorithm to create checksums

### 4. HEAD pointer
* Point to the "tip" of the current checked out branch

## Making changes to files

### 1. Adding files
```
git status (Return the differences between the Working tree, Staging Index and Repository)
```

### 2. Viewing changes with diff
```
git diff (Return the differences in each files between the repository version and the working directory)
git diff <filename> (Return the differences in one specific file)
```

### 3. Viewing only staged changes
```
git diff --staged (Return the differences between the Staging index and Repository)
```
### 4. Deleting files
```
git rm <filename> (Remove the file and add the changes to Staging index)
```
### 5. Moving and renaming files
```
git mv <from_file> <to_file> (Rename or move from one file to another file)
```

## Undoing changes

### 1. Undoing working directory changes
```
git checkout -- <filename> (Stay on the same branch and restore the file backt to what we had in the repostiroy)
```

### 2. Unstaging files
```
git reset HEAD <filename> (Reset to the last commit of current branch)
```

### 3. Amending commits
```
git commit --amend -m "<message>" (Added new changes the most recent commit)
```

### 4. Retrieving old version
```
git checkout <version> -- <filename> (Retrieve the old version)
```

### 5. Reverting a commit
```
git revert <version>
```

### 6. Using reset to undo commits
```
git reset --soft (does not change staging index or working directory)
git reset --mixed (changes staging index to match repository)
git reset --hard (changes staging index and working directory to match repository)
```

### 7. Removing untracked files
```
git clean -n (Show the untracked files would be removed)
git clean -f (Remove the untracked files)
```

## Ignoring files

### 1. Using .gitignore files
* Ignore all the files that are listed in the .gitignore file

### 2. Understanding what to ignore
* compiled source code
* packages and compressed files
* logs and databases
* operating system generated files
* user-uploaded assets (images, PDF, videos)

### 3. Ignoring files globally
```
git config --global core.excludesfile ~/.gitignore_global (tell git what file is for globally ignoring)
```

### 4. Ignoring tracked files
```
git rm --cached <filename> (Remove file fron staging index)
```

### 5. Tracking empty directories
```
touch <path>/.gitkeep (create a file to make empty directory able to be tracked)
```

## Navigating the Commit Tree

### Referencing the commits
```
HEAD^ (go back 1 commit)
HEAD~n (go back n commits)
```

### Exploring tree listings
```
git ls-tree <tree-ish> (list all files in 1 tree-ish)
git ls-tree <tree-ish> <folder>
```

### Viewing commits
```
git show <tree-ish> (Show all the differences)

```

### Comparing commits
```
git diff <tree-ish> (Return the differences between 1 point and current directory)
```

## Branching

### Branching overview
* branches are cheap
* one working directory
* fast context switching

### Viewing and creating branches
```
git branch (Show the current branch)
.git/refs/heads/master store all the branches
git branch <branch-name> (create a new branch)
```

### Switching branches
```
git checkout <branch> (Switch to new branch)
```

### Creating and switching branches
```
git checkout -b <branch> (Create new branch and switch to it)
```

### Switching branches with uncommitted changes
* checkout the files
* commit the files
* use git stash

### Comparing branches
```
git diff <branch1>..<branch2> (Compare the branch1 and branch2)
git diff --color-words (Show the differences in one line)
git branch --merged
```

### Renaming branches
```
git branch -m <old_name> <new_name> (Rename the branch)
```

### Deleting branches
```
git branch -d <branch> (Delete the branch)
git branch -D <branch> (Delete branch that is not fully merged)
```
* Cannot delete which branch you are currently on

## Merging branches

### Merging code
* Check out to the reciever branch
* Merge the sending branch
```
git merge <branch> (merge the branch to the current branch)
```

### Using fast-forward merge vs true merge
* fast-forward: the current branch doesn't have any new commits, so it will move the HEAD to the new branch that is merged

* true merge: the current branch have new commits. It will merge the two branches and create a new commit in the current branch

```
git merge --no-ff <branch> (no fast-forward)
git merge --ff-only <branch> (do fast-forward if possible)
```

### Merging conflicts
* when the two branches have commits in the same files

### Resolving merge conflicts
```
git merge --abort
resolve the conflicts
```

## Stashing Changes

### Saving changes in the stash
```
git stash save "<message>"
```

### Viewing stashed changes
```
git stash list (Return all the stashes)
git stash show <stash name> (Return the changes in one stash)
```

### Retrieving stashed changes
```
git stash apply <stash name> (Pull the copy from the stash)
git stash pop <stash name> (Pull the copy from the stash and remove it)
```

### Deleting stashed changed
```
git stash drop <stash name> (Delete specific stash)
git stash clear <stash name> (Clear all stashes)
```

## Remotes

### Adding a remote repository
```
git remote (-v) (Show all the remotes)
git remote add origin <link> (Create a new remote called origin) 
git remote rm <remote name> (Remove specific remote)
```

### Creating a remote branch
```
git push -u origin
git branch -r (Show the remote branch)
git branch -a (Show all branches)
```

### Clone a remote repository
```
git clone <link> <folder>
```

### Fetching changes from a remote repository
```
git fetch <remote> (synchronize the remote to origin/master)
```
* fetch before you work
* fetch before you push
* fetch often

### Merging in fetched changes
```
git merge <remote bracnh> (Merge to the remote branch)
git pull (fetch and merge chages from the remote branch)
```

### Pushing to an updated remote branch
* 1. fetch
* 2. merge
* 3. resolve conflicts
* 4. push

### A collaboration workflow
```
git checkout master
git fetch
git merge origin/master
git checkout -b <branch>
git add
git commit -m <message>
git fetch
git push -u origin <branch>
```

## Tools and Next Steps

### Using SSH keys for remote login
```
git credential-osxkeychain (Cache the password)
```

Generate SSH key
```
1. cd ~/.ssh
2. ssh-keygen -t rsa -b 4096 -C <email>
```

### Exploring graphical user interfaces
* TortoiseGit, Github, SmartGit

### Understanding Git hosting
* Github
* Bitbucket
* Gitorious 